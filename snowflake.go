package main

import (
	"fmt"
	"strconv"
	"sync"
	"time"
)

var (
	// 2018-11-23 13:09:47.512 UTC
	Epoch    int64 = 1542978587512
	NodeBits uint8 = 10
	StepBits uint8 = 12

	nodeMax   int64 = -1 ^ (-1 << NodeBits)
	nodeMask  int64 = nodeMax << StepBits
	stepMask  int64 = -1 ^ (-1 << StepBits)
	timeShift uint8 = NodeBits + StepBits
	nodeShift uint8 = StepBits
)

type SnowflakeNode struct {
	mutex sync.Mutex
	time  int64
	node  int64
	step  int64
}

type Snowflake struct {
	Timestamp int64 `json:"timestamp"`
	Node      int64 `json:"node"`
	Step      int64 `json:"step"`
}

func NewSnowflakeNode(nodeOpt ...int64) (*SnowflakeNode, error) {
	node := int64(0)
	if len(nodeOpt) > 0 {
		node = nodeOpt[0]
	}

	if node > nodeMax {
		return nil, fmt.Errorf("Node number must be between 0 and %s", strconv.FormatInt(nodeMax, 10))
	}

	return &SnowflakeNode{
		time: 0,
		node: node,
		step: 0,
	}, nil
}

func (node *SnowflakeNode) Generate() *Snowflake {
	node.mutex.Lock()

	getTimeNow := func() int64 {
		return int64(time.Now().UnixNano() / 1000000)
	}

	timeNow := getTimeNow()

	if node.time == timeNow {
		node.step = (node.step + 1) & stepMask
		if node.step == 0 {
			for timeNow <= node.time {
				timeNow = getTimeNow()
			}
		}
	} else {
		node.step = 0
	}

	node.time = timeNow

	snowflake := &Snowflake{
		Timestamp: node.time,
		Node:      node.node,
		Step:      node.step,
	}

	node.mutex.Unlock()

	return snowflake
}

func NewSnowflake(id int64) *Snowflake {
	return &Snowflake{
		Timestamp: (id >> timeShift) + Epoch,
		Node:      (id & nodeMask) >> nodeShift,
		Step:      id & stepMask,
	}
}

func (snowflake *Snowflake) AsInt64() int64 {
	return ((snowflake.Timestamp - Epoch) << timeShift) |
		(snowflake.Node << nodeShift) |
		(snowflake.Step)
}

func (snowflake *Snowflake) AsString() string {
	return fmt.Sprintf("%i", snowflake.AsInt64())
}

func (snowflake *Snowflake) AsBase2() string {
	return strconv.FormatInt(snowflake.AsInt64(), 2)
}

func (snowflake *Snowflake) AsBase16() string {
	return strconv.FormatInt(snowflake.AsInt64(), 16)
}
