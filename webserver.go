package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

type WebServer struct {
	Router *mux.Router
	Nodes  map[int]*SnowflakeNode
}

type APIError struct {
	Code    int
	Message string
}

func OpenWebServer(port int) {
	sport := strconv.Itoa(port)
	log.Println("WebServer listening on port " + sport)
	router := mux.NewRouter()
	webServer := &WebServer{
		Router: router,
		Nodes:  make(map[int]*SnowflakeNode),
	}
	webServer.registerHandlers()
	http.Handle("/", webServer.Router)
	http.ListenAndServe(":"+sport, nil)
}

func (ws *WebServer) registerHandlers() {

	// GET /
	ws.Router.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		writeJSONData(w, map[string]interface{}{
			"epoch": Epoch,
			"endpoints": []string{
				"GET    /nodes",
				"POST   /nodes/:nodeID",
				"DELETE /nodes/:nodeID",
				"POST   /nodes/:nodeID/snowflake",
				"GET    /snowflakeinfo/:snowflakeInt64",
			},
		})
	}).Methods("GET")

	// GET /nodes
	ws.Router.HandleFunc("/nodes", func(w http.ResponseWriter, r *http.Request) {
		keys := make([]int, len(ws.Nodes))
		c := 0
		for k := range ws.Nodes {
			keys[c] = k
			c++
		}
		writeJSONData(w, map[string]interface{}{
			"n":     len(ws.Nodes),
			"nodes": keys,
		})
	}).Methods("GET")

	// POST /nodes/:nodeID
	ws.Router.HandleFunc("/nodes/{node:[0-9]+}", func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)
		nodeN, err := strconv.Atoi(vars["node"])
		if err != nil {
			writeAPIError(w, err.Error(), http.StatusBadRequest)
			return
		}

		node, err := NewSnowflakeNode(int64(nodeN))
		if err != nil {
			writeAPIError(w, err.Error(), http.StatusBadRequest)
			return
		}

		ws.Nodes[nodeN] = node
		writeJSONData(w, map[string]interface{}{
			"epoch": Epoch,
			"node":  nodeN,
		}, http.StatusCreated)

	}).Methods("POST")

	// DELETE /nodes/:nodeID
	ws.Router.HandleFunc("/nodes/{node:[0-9]+}", func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)
		nodeN, err := strconv.Atoi(vars["node"])
		if err != nil {
			writeAPIError(w, err.Error(), http.StatusBadRequest)
			return
		}

		if _, ok := ws.Nodes[nodeN]; ok {
			delete(ws.Nodes, nodeN)
			w.WriteHeader(http.StatusOK)
		} else {
			writeAPIError(w, fmt.Sprintf("Node %d does not exist", nodeN), http.StatusBadRequest)
		}

	}).Methods("DELETE")

	// POST /nodes/:nodeID/snowflake
	ws.Router.HandleFunc("/nodes/{node:[0-9]+}/snowflake", func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)
		nodeN, err := strconv.Atoi(vars["node"])
		if err != nil {
			writeAPIError(w, err.Error(), http.StatusBadRequest)
			return
		}

		if node, ok := ws.Nodes[nodeN]; ok {
			snowflake := node.Generate()
			writeJSONData(w, map[string]interface{}{
				"snowflake": snowflake,
				"int64":     snowflake.AsInt64(),
				"base2":     snowflake.AsBase2(),
				"base16":    snowflake.AsBase16(),
			})
		} else {
			writeAPIError(w, fmt.Sprintf("Node %d does not exist", nodeN), http.StatusBadRequest)
		}

	}).Methods("POST")

	// GET /snowflakeinfo/:snowflakeInt64
	ws.Router.HandleFunc("/snowflakeinfo/{snowflake:[0-9]+}", func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)
		snowflakeID, err := strconv.Atoi(vars["snowflake"])
		if err != nil {
			writeAPIError(w, err.Error(), http.StatusBadRequest)
			return
		}
		writeJSONData(w, NewSnowflake(int64(snowflakeID)))
	}).Methods("GET")

}

// PRIVATES --------------------------------------------------------------------------

func writeJSONData(w http.ResponseWriter, obj interface{}, statusCode ...int) error {
	w.Header().Set("Content-Type", "application/json")
	if len(statusCode) > 0 {
		w.WriteHeader(statusCode[0])
	} else {
		w.WriteHeader(http.StatusOK)
	}
	encoder := json.NewEncoder(w)
	encoder.SetIndent("", "  ")
	return encoder.Encode(obj)
}

func writeAPIError(w http.ResponseWriter, message string, statusCode int) {
	writeJSONData(w, APIError{
		Code:    statusCode,
		Message: message,
	}, statusCode)
}
