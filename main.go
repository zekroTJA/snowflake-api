package main

import (
	"flag"
)

var (
	port = flag.Int("p", 8080, "Expose port of API")
)

func main() {
	flag.Parse()
	OpenWebServer(*port)
}
